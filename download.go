package main

import (
	"context"
	"encoding/gob"
	"fmt"

	"cloud.google.com/go/storage"
)

type client struct {
	*storage.Client
	bucket string
}

func main() {
	ctx := context.Background()

	c, err := storage.NewClient(ctx)
	if err != nil {
		panic(err)
	}

	gcs := client{Client: c, bucket: bucket}

	var v1 type1
	var v2 type2

	if err := gcs.fetch(ctx, file1, &v1); err != nil {
		panic(err)
	}
	if err := gcs.fetch(ctx, file2, &v2); err != nil {
		panic(err)
	}

	fmt.Printf("%s: %+v\n", file1, v1)
	fmt.Printf("%s: %+v\n", file2, v2)
}

func (c *client) fetch(ctx context.Context, path string, v interface{}) error {
	r, err := c.Bucket(c.bucket).Object(path).NewReader(ctx)
	if err != nil {
		return err
	}
	defer r.Close()

	return gob.NewDecoder(r).Decode(v)
}
