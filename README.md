Example: gob on GCS in golang
=============================

```bash
$ make upload
go run common.go upload.go

$ make download
go run common.go download.go
type1-value.gob: map[1:1 2:2]
type2-value.gob: map[1:map[1:1 2:2] 2:map[1:2 2:4]]
```
