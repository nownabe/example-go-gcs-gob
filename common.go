package main

const (
	bucket = "nownabe-dev-1"
	file1  = "type1-value.gob"
	file2  = "type2-value.gob"
)

type type1 map[int]int
type type2 map[int]map[int]float32
