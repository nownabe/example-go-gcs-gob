package main

import (
	"bytes"
	"context"
	"encoding/gob"

	"cloud.google.com/go/storage"
)

func main() {
	v1 := type1{
		1: 1,
		2: 2,
	}

	v2 := type2{
		1: map[int]float32{
			1: 1.0,
			2: 2.0,
		},
		2: map[int]float32{
			1: 2.0,
			2: 4.0,
		},
	}

	var buf1, buf2 bytes.Buffer

	if err := gob.NewEncoder(&buf1).Encode(v1); err != nil {
		panic(err)
	}
	if err := gob.NewEncoder(&buf2).Encode(v2); err != nil {
		panic(err)
	}

	ctx := context.Background()

	client, err := storage.NewClient(ctx)
	if err != nil {
		panic(err)
	}
	w1 := client.Bucket(bucket).Object(file1).NewWriter(ctx)
	w2 := client.Bucket(bucket).Object(file2).NewWriter(ctx)

	if _, err := w1.Write(buf1.Bytes()); err != nil {
		panic(err)
	}
	if _, err := w2.Write(buf2.Bytes()); err != nil {
		panic(err)
	}

	if err := w1.Close(); err != nil {
		panic(err)
	}
	if err := w2.Close(); err != nil {
		panic(err)
	}
}
